# TP Realworld
Créer un nouveau paragraphe pour chaque section 6.x.y
## 6.3
### 6.3.1
SHA1 : 7a2e472d9d9e77ad7de8f4ace9e09d7597ee60f7

## 6.3.2
https://github.com/mkenney/docker-npm/blob/master/debian/Dockerfile

1 seul volume déclaré : /src
docker pull mkenney/npm:node-8-debian
image id 9af19a7f4e2c
## 6.3.3
commande : docker container run -ti --rm -v $(pwd):/src mkenney/npm:node-8-debian npm install
## 6.3.4
commande : docker container run -ti --rm -v $(pwd):/src -p 8000:8080 mkenney/npm:node-8-debian npm run dev

## 6.4.1
SHA1 : 5a11b9d391970a9be59d72f7e069b65c81f2118e
## 6.4.2 
commande : docker pull gradle:4.7.0-jdk8-alpine
image id : f438b7d58d0a 
## 6.4.3
docker volume create gradle-home
docker volume ls
DRIVER              VOLUME NAME
local               gradle-home
## 6.4.4
docker container run -ti --rm -v $(pwd):/src -v gradle-home:/home/gradle/.gradle -w /src gradle:4.7.0-jdk8-alpine gradle build
## 6.4.5
sudo docker container run -ti --rm -v $(pwd):/src -v gradle-home:/home/gradle/.gradle -w /src -p 8001:8080 gradle:4.7.0-jdk8-alpine gradle bootRun
{"articles":[],"articlesCount":0}
## 6.5.1
{"articles":[{"id":"23401a7c-6957-40e1-a2bb-5e83f75bf7cd","slug":"hop","title":"hop","description":"bim","body":"tac","favorited":false,"favoritesCount":0,"createdAt":"2018-05-16T12:48:36.068Z","updatedAt":"2018-05-16T12:48:36.068Z","tagList":[],"author":{"username":"bob","bio":"","image":"https://static.productionready.io/images/smiley-cyrus.jpg","following":false}}],"articlesCount":1}
## 6.6.1
sudo docker container run -ti --rm -v $(pwd):/src mkenney/npm:node-8-debian npm run build
favicons-c2a605fbc0e687b2e1b4b90a7c445cdd  index.html  static
## 6.6.2
sudo docker container run -ti --rm -v $(pwd):/usr/share/nginx/html -p 8000:80 nginx:alpine
## 6.6.3
sudo docker image build --build-arg SHA1=7a2e472d9d9e77ad7de8f4ace9e09d7597ee60f7 -t realworldfront:vuejs .
https://hub.docker.com/r/bast3t/realworldfront/